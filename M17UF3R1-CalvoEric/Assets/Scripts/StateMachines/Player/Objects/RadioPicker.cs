using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioPicker : MonoBehaviour
{
    public bool inTrigger;



    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Radio.radio = true;
                Destroy(this.gameObject);
            }
        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            GUI.Box(new Rect(0, 0, 200, 25), "Presiona la E para coger la Radio");
        }
    }
}
