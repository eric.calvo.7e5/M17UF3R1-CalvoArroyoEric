using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{
    public static bool radio;
    public bool start;
    public bool inTrigger;
    public GameObject win;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            if (radio)
            {
                GUI.Box(new Rect(0, 0, 200, 25), "Presiona la tecla E para empezar a transmitir tu se�al");
                Invoke("WinScene", 10f);
            }
            else
            {
                GUI.Box(new Rect(0, 0, 200, 25), "�Necesitas una radio!");
            }
        }
    }

    public void WinScene()
    {
        win.SetActive(true);
    }
}


