using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDanceState : PlayerBaseState
{
    private readonly int DanceHash = Animator.StringToHash("Celebration");
    private const float CrossFadeDuration = 0.1f;
    public PlayerDanceState(PlayerStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void Enter()
    {
        
        stateMachine.animator.CrossFadeInFixedTime(DanceHash, CrossFadeDuration);
    }
    public override void Tick(float deltaTime)
    {
        
        //if (stateMachine.animator.)
        //{
        //    ReturnToLocomotion();
        //}
        
    }

    public void Timer()
    {

    }
    public override void Exit()
    {
       
    }

    

}
