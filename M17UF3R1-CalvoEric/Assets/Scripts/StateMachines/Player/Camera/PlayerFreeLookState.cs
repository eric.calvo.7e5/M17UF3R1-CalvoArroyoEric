using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFreeLookState : PlayerBaseState
{

    private readonly int FreeLookSpeedHash = Animator.StringToHash("FreeLookSpeed");
    private const float AnimatorDampTime = 0.1f;
    public PlayerFreeLookState(PlayerStateMachine stateMachine) : base(stateMachine)    {   }
    public override void Enter()
    {
        stateMachine.InputReader.JumpEvent += OnJump;
        stateMachine.InputReader.DanceEvent += OnDance;
        stateMachine.InputReader.AimEvent += OnAim;

    }
    public override void Tick(float deltaTime)
    {
        Vector3 movement = CalculateMovement();

        Move(movement * stateMachine.FreeLookMovementSpeed, deltaTime);

        if (stateMachine.InputReader.MovementValue == Vector2.zero) 
        {
            stateMachine.animator.SetFloat(FreeLookSpeedHash, 0, AnimatorDampTime, deltaTime);
            return; 
        }
        stateMachine.animator.SetFloat(FreeLookSpeedHash, 0.3333333f, AnimatorDampTime, deltaTime);
        FaceMovementDirection(movement, deltaTime);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            stateMachine.animator.SetFloat(FreeLookSpeedHash, 1f, AnimatorDampTime, deltaTime);
            
            if (stateMachine.FreeLookMovementSpeed <= 20.0f)
            {
                stateMachine.FreeLookMovementSpeed = 20f;
            }
            else
            {
                stateMachine.FreeLookMovementSpeed += 10f * deltaTime;
            }
        }
        else
        {
            stateMachine.FreeLookMovementSpeed = 5f;
        }
    }
    public override void Exit()
    {
        stateMachine.InputReader.JumpEvent -= OnJump;
        stateMachine.InputReader.DanceEvent -= OnDance;
        stateMachine.InputReader.AimEvent -= OnAim;
    }

    private Vector3 CalculateMovement()
    {
        Vector3 forward = stateMachine.MainCameraTransform.forward;
        Vector3 right = stateMachine.MainCameraTransform.right;

        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        return forward * stateMachine.InputReader.MovementValue.y +
            right * stateMachine.InputReader.MovementValue.x;
    }

    private void FaceMovementDirection(Vector3 movement, float deltaTime)
    {
        stateMachine.transform.rotation = Quaternion.Lerp(
            stateMachine.transform.rotation,
            Quaternion.LookRotation(movement),
            deltaTime * stateMachine.RotationDamping);
    }
    private void OnAim()
    {
        stateMachine.SwitchState(new PlayerAimingState(stateMachine));
    }

    private void OnJump()
    {
        stateMachine.SwitchState(new PlayerJumpState(stateMachine));
    }

    private void OnDance()
    {
        stateMachine.SwitchState(new PlayerDanceState(stateMachine));
    }

}
