using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimingState : PlayerBaseState
{

    private Transform cameraTransform;
    [SerializeField] private float rotationSpeed = .5f;
    private readonly int PistolIdleHash = Animator.StringToHash("Pistol Idle");
    private const float AnimatorDampTime = 0.1f;
    public PlayerAimingState(PlayerStateMachine stateMachine) : base(stateMachine)
    {

    }

    public override void Enter()
    {
        
        cameraTransform = Camera.main.transform;
    }

    public override void Tick(float deltaTime)
    {
        Vector3 movement = CalculateMovement();
        Quaternion targetRotation = Quaternion.Euler(0, cameraTransform.eulerAngles.y, 0);
        stateMachine.transform.rotation = Quaternion.Lerp(stateMachine.transform.rotation, targetRotation, rotationSpeed*Time.deltaTime);
    }

    public override void Exit()
    {

    }

    private Vector3 CalculateMovement()
    {
        Vector3 forward = stateMachine.MainCameraTransform.forward;
        Vector3 right = stateMachine.MainCameraTransform.right;

        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        return forward * stateMachine.InputReader.MovementValue.y +
            right * stateMachine.InputReader.MovementValue.x;
    }
}
