using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class SwitchVCAM : MonoBehaviour
{
    [SerializeField]
    private InputReader playerInput;
    public CinemachineFreeLook freeLookCam;
    public CinemachineVirtualCamera virtualCamera;
    private InputAction aimAction;
    public Animator anim;
    private void Awake()
    {
        
    }

    private void OnEnable()
    {
        playerInput.AimEvent += StartAim;
    }

    private void OnDisable()
    {
        playerInput.AimEvent -= StartAim;
    }

    private void StartAim()
    {
        if (playerInput.IsAiming)
        {
            freeLookCam.Priority = 8;
            
        }
        else
        {
            freeLookCam.Priority = 10;
        }

    }

}
