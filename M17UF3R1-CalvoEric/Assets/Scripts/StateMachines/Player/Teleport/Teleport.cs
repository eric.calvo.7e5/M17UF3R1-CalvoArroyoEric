using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public GameObject Inside;
    public GameObject Outside;
    public GameObject OutsideDoor;
    public GameObject InsideDoor;
    void Start()
    {
        Inside = GameObject.Find("InPoint");
        Outside = GameObject.Find("OutPoint");
        OutsideDoor = GameObject.FindGameObjectWithTag("Out");
        InsideDoor = GameObject.FindGameObjectWithTag("In");
}

// Update is called once per frame
void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.tag == "In")
        {
            transform.position = Outside.transform.position;
        }
        if (other.tag == "Out")
        {
           transform.position = Inside.transform.position;
        }
    }
}
