using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class EnemyDeadState : EnemyBaseState
{
    public EnemyDeadState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void Enter()
    {
        stateMachine.Weapon.gameObject.SetActive(false);
        stateMachine.animator.CrossFadeInFixedTime("Death", 0.1f);
        stateMachine.MovementSpeed = 0f;
    }
    public override void Tick(float deltaTime)
    {

    }

    public override void Exit()
    {

    }


}
