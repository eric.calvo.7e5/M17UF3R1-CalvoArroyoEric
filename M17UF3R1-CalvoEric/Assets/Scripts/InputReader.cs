using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputReader : MonoBehaviour, Controls.IPlayerActions
{
    public bool IsAttacking { get; set; }
    public bool IsDancing { get; set; }
    public bool IsAiming { get; set; }
    public Vector2 MovementValue { get; private set; }
    public event Action JumpEvent;
    public event Action DodgeEvent;
    public event Action MoveEvent;
    public event Action DanceEvent;
    public event Action ChangeWeaponEvent;
    public event Action InteractionEvent;
    public event Action AimEvent;
    public event Action ShootAttackEvent;
    private Controls controls;

    private void Start()
    {
        controls = new Controls();
        controls.Player.SetCallbacks(this);

        controls.Player.Enable();
    }

    private void OnDestroy()
    {
        controls.Player.Disable();

    }
    public void OnJump(InputAction.CallbackContext context)
    {
        if (!context.performed) { return; }

        JumpEvent?.Invoke();
    }

    public void OnDodge(InputAction.CallbackContext context)
    {
        if (!context.performed) { return; }

        DodgeEvent?.Invoke();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        MovementValue = context.ReadValue<Vector2>();
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        //lo usa el cinemachine
    }

    public void OnDance(InputAction.CallbackContext context)
    {
        if (!context.performed) { return; }

        DanceEvent?.Invoke();
        if (context.phase == InputActionPhase.Started)
        {
            IsDancing = true;
        }
    }

    public void OnChangeWeapon(InputAction.CallbackContext context)
    {
        if (!context.performed) { return; }

        ChangeWeaponEvent?.Invoke();
    }

    public void OnAim(InputAction.CallbackContext context)
    {
        Debug.Log(context.ToString());
        AimEvent?.Invoke();
        if (context.phase == InputActionPhase.Started)
        {
            IsAiming = true;
        }
        else
        {
            IsAiming = false;
        }
        
        
    }

    public void OnInteract(InputAction.CallbackContext context)
    {

    }

    public void OnShootAttack(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            IsAttacking = true;
        }else if(context.canceled)
        {
            IsAttacking = false;
        }
    }

    public void OnSprint(InputAction.CallbackContext context)
    {
        
    }
}
